# import libraries
import numpy as np
import pandas as pd
import streamlit as st
import logging as lg
import time


st.title('Selection of perceptron hyperparameters')

def perf (x, y, z):
    perf_list = []
    epoches_list = []
    time_list = []
    for i in range(1, epochs+1):
        perf = int(2*x - i + 3*z)
        x_time = time.time()
        perf_list.append(perf)
        epoches_list.append(i)
        time_list.append(x_time)
        perf_df = pd.DataFrame({
            'time': time_list,
            'epoches': epoches_list,
            'perf': perf_list
        })

    return perf_df



#Creating and Configuring Logger
Log_Format = "%(levelname)s, %(asctime)s, %(message)s"

lg.basicConfig(filename = "logfile.csv",
                    filemode = "w",
                    format = Log_Format, 
                    level = lg.INFO)

logger = lg.getLogger()


# Создаем боковую панель
with st.sidebar:
    st.write('Select initial values')
    x = st.slider('x', min_value=0, max_value=10, value=1, step=1)
    y = st.slider('y', min_value=0, max_value=10, value=1, step=1)
    z = st.slider('z', min_value=0, max_value=10, value=1, step=1)
    epochs = st.slider('epochs', min_value=0, max_value=10, value=4, step=1)

# Тестова функция 
performance = int(2*x - y + 3*z)

test = perf(x, y, z)
st.write(test)

# Простой набор данных для вычисления дельты построения графика
dataf4 = pd.DataFrame({
    'epochs': [4, 5, 6, 7],
    'Y': [8, 9, 10, 11],
    'hidden_nodes': [1, 2, 3, 4]
    },
    #index=[0]
    )

var_results = pd.DataFrame({
    'epochs': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
    'performance': [0.9551, 0.9664, 0.9692, 0.9718, 0.9743, 0.9734, 0.9741, 0.9731, 0.9731, 0.9745, 0.9732, 0.9735, 0.9738, 0.9743, 0.9721]
    },
    #index=[0]
    )

# Записываем в лог необходимые переменные
log_file = logger.info(f"performance - {performance}, epochs - {x}, learning rate - {y}")

# Вычисляем дельту
last_num = dataf4.loc[len(dataf4)-1, 'epochs']
prev_num = dataf4.loc[len(dataf4)-2, 'epochs']
delta_var = int(last_num - prev_num)



# Добавляем значение метрики в боковую панель
with st.sidebar:
    st.metric(label="Resulting performance", value=performance)

# Строим график на основе "записанного" набора данных
st.line_chart(data=test, x='epoches', y='perf', width=0, height=0, use_container_width=True)