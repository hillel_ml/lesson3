import logging

x = 1
y = 2

#Creating and Configuring Logger

Log_Format = "%(levelname)s, %(asctime)s, %(message)s"

logging.basicConfig(filename = "logfile.csv",
                    filemode = "w",
                    format = Log_Format, 
                    level = logging.INFO)

logger = logging.getLogger()

#Testing our Logger

logger.info(f"accuracy, {x}, level, {y}")