import numpy as np
import pandas as pd
import streamlit as st
import logging

#Creating and Configuring Logger

Log_Format = "%(message)s"

logging.basicConfig(filename = "logfile.csv",
                    filemode = "w",
                    format = Log_Format, 
                    level = logging.INFO)

logger = logging.getLogger()





def performance (x, y, z):
    perf_list = []
    perf = x + y + z
    for i in range(2):
        perf_list.append(perf)

    return perf_list

x = st.slider('x', min_value=0, max_value=10, value=1, step=1)
y = st.slider('y', min_value=0, max_value=10, value=1, step=1)
z = st.slider('z', min_value=0, max_value=10, value=1, step=1)
# performance = x + y + z

# x = 1
# y = 1
# z = 1

test = performance(x, y, z)

# test.append(test)

# print(test)

st.write(test)

#Testing our Logger

log_file = logger.info(f"x is, {x}, y is, {y}")