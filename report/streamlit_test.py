import streamlit as st
import pandas as pd
import numpy
import logging as lg
import time
import scipy.special

st.title('Selection of perceptron hyperparameters')

# Создаём пустые списки и словари для дальнейшего использования
perf_list = []
epoches_list = []
dict_train = {}
dict_weights_wih = {}
dict_weights_who = {}
dict_def_perf = {}

# Создание пустого накопителя для оценки качества
scorecard = []

# Загрузка тренировочного набора данных
training_data_file = open("mnist_train.csv", 'r')
training_data_list = training_data_file.readlines()
training_data_file.close()

# Загрузка тестового набора данных
test_data_file = open("mnist_test.csv", 'r')
test_data_list = test_data_file.readlines()
test_data_file.close()


# Создаем функцию для расчёта scorecard (оценочная карта?)
def perf (epochs):
    for e in range(epochs):
        # итерирование по всем записям обучающего набора
        for record in training_data_list:
            # разделение записей по запятым ','
            all_values = record.split(',')
            # масштабирование и сдвиг исходных данных
            inputs = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
            # создание целевых  выходов
            targets = numpy.zeros(output_nodes) + 0.01
            # элемент all_values[0] является целевым для этой записи
            targets[int(all_values[0])] = 0.99
            n.train(inputs, targets)
    # Тестирование нейронной сети
    # итерирование по тестовому набору данных
    for record in test_data_list:
        # разделение записей по запятым ','
        all_values = record.split(',')
        # правильный ответ - в первой ячейке
        correct_label = int(all_values[0])
        # масштабирование и сдвиг исходных данных
        inputs2 = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
        # получение ответа от нейронной сети
        outputs = n.query(inputs2)
        # получение выхода
        label = numpy.argmax(outputs)
        # добавление в список единицы, если ответ совпал с целевым значением
        if (label == correct_label):
            scorecard.append(1)
        else:
            scorecard.append(0)
            pass
    dict_def_perf[e] = [scorecard] # для словаря выбирается единственное и последнее значение переменной "e"
    return dict_def_perf


# описание класса нейронной сети
class neuralNetwork:

    # инициализация нейронной сети
    def __init__(self, inputnodes, hiddennodes, outputnodes, learningrate):
        # задание количества узлов входного, скрытого и выходного слоя
        self.inodes = inputnodes
        self.hnodes = hiddennodes
        self.onodes = outputnodes

        # связь весовых матриц, wih и who
        # вес внутри массива w_i_j, где связь идет из узла i в узел j
        # следующего слоя
        # w11 w21
        # w12 w22 и т д7
        self.wih = numpy.random.normal(0.0, pow(self.inodes, -0.5), (self.hnodes, self.inodes))
        self.who = numpy.random.normal(0.0, pow(self.hnodes, -0.5), (self.onodes, self.hnodes))

        # уровень обучения
        self.lr = learningrate

        # функция активации - сигмоид
        self.activation_function = lambda x: scipy.special.expit(x)
        pass

        # обучение нейронной сети

    def train(self, inputs_list, targets_list):
        # преобразование входного списка 2d массив
        inputs = numpy.array(inputs_list, ndmin=2).T
        targets = numpy.array(targets_list, ndmin=2).T

        # вычисление сигналов на входе в скрытый слой
        hidden_inputs = numpy.dot(self.wih, inputs)
        # вычисление сигналов на выходе из скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)

        # вычисление сигналов на входе в выходной слой
        final_inputs = numpy.dot(self.who, hidden_outputs)
        # вычисление сигналов на выходе из выходного слоя
        final_outputs = self.activation_function(final_inputs)

        # ошибка на выходе (целевое значение - рассчитанное)
        output_errors = targets - final_outputs
        # распространение ошибки по узлам скрытого слоя
        hidden_errors = numpy.dot(self.who.T, output_errors)

        # пересчет весов между скрытым и выходным слоем
        self.who += self.lr * numpy.dot((output_errors * final_outputs * (1.0 - final_outputs)),
                                        numpy.transpose(hidden_outputs))

        # пересчет весов между входным и скрытым слоем
        self.wih += self.lr * numpy.dot((hidden_errors * hidden_outputs * (1.0 - hidden_outputs)),
                                        numpy.transpose(inputs))

        # dict_weights_wih[time.time()] = [self.wih]
        # dict_weights_who[time.time()] = [self.who]
        pass

    # запрос к нейронной сети
    def query(self, inputs_list):
        # преобразование входного списка 2d массив
        inputs = numpy.array(inputs_list, ndmin=2).T

        # вычисление сигналов на входе в скрытый слой
        hidden_inputs = numpy.dot(self.wih, inputs)
        # вычисление сигналов на выходе из скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)

        # вычисление сигналов на входе в выходной слой
        final_inputs = numpy.dot(self.who, hidden_outputs)
        # вычисление сигналов на выходе из выходного слоя
        final_outputs = self.activation_function(final_inputs)

        return final_outputs


# Задание архитектуры сети:
# количество входных, скрытых и выходных узлов
input_nodes = 784
with st.sidebar:
    st.write('Setting the neural network architecture:')
    hidden_nodes = st.slider('Set the number of hidden layers', min_value=0, max_value=300, value=250, step=10)
output_nodes = 10


# уровень обучения
with st.sidebar:
    power = st.slider('Set the power of learning level', min_value=0.0, max_value=10.0, value=1.0, step=1.0)
    value = st.slider('Set the value of learning level', min_value=0.0, max_value=10.0, value=2.0, step=1.0)
    
learning_rate = value * 10 ** (-power)


# создание экземпляра класса нейронной сети
n = neuralNetwork(input_nodes,
                  hidden_nodes,
                  output_nodes,
                  learning_rate)

# Обучение нейронной сети
# количество эпох
with st.sidebar:
    epochs = st.slider('Set the number of epochs', min_value=0, max_value=15, value=5, step=1)


# Вызываем функцию и передаем ем количество эпох
perf(epochs)

# расчет точности классификатора
scorecard_array = numpy.asarray(scorecard)
performance_var = scorecard_array.sum() / scorecard_array.size

var_delta = performance_var - 0.9745

# Выводим на печать словарь после расчета точности классификатора
st.write(dict_def_perf)


# Вывод финального значения точности классификатора
with st.sidebar:
    st.metric(label="Performance", value=performance_var, delta=var_delta)


var_results = pd.DataFrame({
    'epoches': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
    'performance': [0.9551, 0.9664, 0.9692, 0.9718, 0.9743, 0.9734, 0.9741, 0.9731, 0.9731, 0.9745, 0.9732, 0.9735, 0.9738, 0.9743, 0.9721]
    },
    #index=[0]
    )

# Строим график на основе "записанного" набора данных
st.line_chart(data=var_results, x='epoches', y='performance', width=0, height=0, use_container_width=True)





# -----------------------------------------------------------------------------------------
# Запчасти


# st.line_chart(x=epochs, y=performance_var, width=0, height=0, use_container_width=True)



# # Попытка записать номера эпох и результаты расчета точности классификатора в списки
# perf_list.append(performance_var)
# epoches_list.append(epochs)



# # Записывем номера эпох и результаты расчета точности классификатора в таблицу
# perf_df = pd.DataFrame({
#     'epoches': epoches_list,
#     'perf': perf_list
# })



# #Creating and Configuring Logger
# Log_Format = "%(levelname)s, %(asctime)s, %(message)s"

# lg.basicConfig(filename = "logfile.csv",
#                     filemode = "w",
#                     format = Log_Format, 
#                     level = lg.INFO)

# logger = lg.getLogger()



# # Записываем в лог необходимые переменные
# log_file = logger.info(f"performance - {performance}, epochs - {epochs}, learning rate - {learning_rate}")


# # Вычисляем дельту
# last_num = dataf4.loc[len(dataf4)-1, 'epochs']
# prev_num = dataf4.loc[len(dataf4)-2, 'epochs']
# delta_var = int(last_num - prev_num)