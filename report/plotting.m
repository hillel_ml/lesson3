clc

epoches = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
performance = [0.9551, 0.9664, 0.9692, 0.9718, 0.9743, 0.9734, 0.9741, 0.9731, 0.9731, 0.9745, 0.9732, 0.9735, 0.9738, 0.9743, 0.9721];

plot(epoches, performance)
xlim([0 15]);
xlabel('# epoches');
ylabel('performance');
xticks(0:1:15);
grid on