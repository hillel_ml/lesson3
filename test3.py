# Создаем функцию для расчёта scorecard (оценочная карта?)
def perf (epochs):
    # итерирование по всем записям обучающего набора
    for record in training_data_list:
        # разделение записей по запятым ','
        all_values = record.split(',')
        # масштабирование и сдвиг исходных данных
        inputs = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
        # создание целевых  выходов
        targets = numpy.zeros(output_nodes) + 0.01
        # элемент all_values[0] является целевым для этой записи
        targets[int(all_values[0])] = 0.99
        n.train(inputs, targets)
    # Тестирование нейронной сети
    # итерирование по тестовому набору данных
    for record in test_data_list:
        # разделение записей по запятым ','
        all_values = record.split(',')
        # правильный ответ - в первой ячейке
        correct_label = int(all_values[0])
        # масштабирование и сдвиг исходных данных
        inputs2 = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
        # получение ответа от нейронной сети
        outputs = n.query(inputs2)
        # получение выхода
        label = numpy.argmax(outputs)
        # добавление в список единицы, если ответ совпал с целевым значением
        if (label == correct_label):
            scorecard.append(1)
        else:
            scorecard.append(0)
            pass
    dict_def_perf[e] = [scorecard] # для словаря выбирается единственное и последнее значение переменной "e"
    return dict_def_perf