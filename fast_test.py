# import libraries
import numpy as np
import pandas as pd
import streamlit as st
import logging as lg


#Creating and Configuring Logger

Log_Format = "%(levelname)s, %(asctime)s, %(message)s"

lg.basicConfig(filename = "logfile.csv",
                    filemode = "w",
                    format = Log_Format, 
                    level = lg.INFO)

logger = lg.getLogger()


with st.sidebar:
    st.write('Select initial values')
    x = st.slider('x', min_value=0, max_value=10, value=1, step=1)
    y = st.slider('y', min_value=0, max_value=10, value=1, step=1)
    z = st.slider('z', min_value=0, max_value=10, value=1, step=1)
    
performance = x + y + z

dataf2 = pd.DataFrame({
    'epochs': [1, 2, 3, 4],
    'Y': [1, 2, 3, 4],
    'hidden_nodes': [1, 2, 3, 4],
    'Performance': [1, 2, 3, 4]
    },
    #index=[0]
    )


dataf3 = pd.DataFrame({
    'epochs': x,
    'Y': y,
    'hidden_nodes': z,
    'Performance': performance
    },
    index=[0]
    )

dataf4 = pd.DataFrame({
    'epochs': [4, 5, 6, 7],
    'Y': [8, 9, 10, 11],
    'hidden_nodes': [1, 2, 3, 4]
    },
    #index=[0]
    )

#Testing our Logger
# log_file = logger.info(f"performance is, {performance}")
# logger.info(f"{performance},{delta_var}")
# logger.info(f"test, {x}")

log_file = logger.info(f"accuracy, {x}, level, {y}")

st.write(log_file)

# dataf5 = pd.read_csv('logs.txt')
# st.write(dataf5)

last_num = dataf4.loc[len(dataf4)-1, 'epochs']
prev_num = dataf4.loc[len(dataf4)-2, 'epochs']
delta_var = int(last_num - prev_num)


# len_var = len(dataf4)-1

# st.write(dataf4.loc[len_var,'epochs'])
# st.write(delta_var)

dataf2.loc[dataf2.shape[0]] = [x, y, z, performance]

dataf2 = pd.concat([dataf2, dataf3], ignore_index = True, axis = 0)

# epochs = dataf2[epochs]

st.dataframe(data=dataf2)
st.dataframe(data=dataf3)

# dataf2.iterrows(0)

with st.sidebar:
    st.metric(label="Performance", value=performance, delta=delta_var)

len_data = len(dataf2['epochs'])

st.write(len_data)

st.line_chart(data=dataf4, width=0, height=0, use_container_width=True)


